package com.example.projet_android;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;


public class DemanderStageActivity extends AppCompatActivity {
    private static final int PICK_FILE_REQUEST_CODE = 100;

    private EditText etNom, etPrenom, etTelephone, etEmail, etNiveauEtude, etSpecialite, etCV;
    private Button btnEnvoyerDemande, btnChooseFile;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demander_stage);


        // Initialize Firebase
        db = FirebaseFirestore.getInstance();

        etNom = findViewById(R.id.etNom);
        etPrenom = findViewById(R.id.etPrenom);
        etTelephone = findViewById(R.id.etTelephone);
        etEmail = findViewById(R.id.etEmail);
        etNiveauEtude = findViewById(R.id.etNiveauEtude);
        etSpecialite = findViewById(R.id.etSpecialite);
        etCV = findViewById(R.id.etCV);
        btnEnvoyerDemande = findViewById(R.id.btnEnvoyerDemande);
        btnChooseFile = findViewById(R.id.btnChooseFile);

        btnChooseFile.setOnClickListener(view -> openFilePicker());

        // Set click listener for envoyer demande button
        btnEnvoyerDemande.setOnClickListener(view -> {
            // Get the form data
            String nom = etNom.getText().toString();
            String prenom = etPrenom.getText().toString();
            String telephone = etTelephone.getText().toString();
            String email = etEmail.getText().toString();
            String niveauEtude = etNiveauEtude.getText().toString();
            String specialite = etSpecialite.getText().toString();
            String cvPath = etCV.getText().toString();

            // Check if all fields are filled
            if (nom.isEmpty() || prenom.isEmpty() || telephone.isEmpty() || email.isEmpty() || niveauEtude.isEmpty() || specialite.isEmpty() || cvPath.isEmpty()) {
                showToast("Veuillez remplir tous les champs.");
                return;
            }

            // Create a data model for the stage information
            Demande demande = new Demande(nom, prenom, telephone, email, niveauEtude, specialite, cvPath);

            // Save the information to Firestore
            savedemandeToFirestore(demande);
        });
    }

    private void openFilePicker() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*"); // All file types
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        ActivityCompat.startActivityForResult(this, Intent.createChooser(intent, "Choose a file"), PICK_FILE_REQUEST_CODE, null);
    }

    private void savedemandeToFirestore(Demande demande) {
        // Create a map from the Stage object
        Map<String, Object> stageMap = new HashMap<>();
        stageMap.put("nom", demande.getNom());
        stageMap.put("prenom", demande.getPrenom());
        stageMap.put("telephone", demande.getTelephone());
        stageMap.put("email", demande.getEmail());
        stageMap.put("niveauEtude", demande.getNiveauEtude());
        stageMap.put("specialite", demande.getSpecialite());
        stageMap.put("cvPath", demande.getCvPath());

        // Add a new document with a generated ID
        db.collection("demandes")
                .add(stageMap)
                .addOnSuccessListener(documentReference -> {
                    showToast("demande ajouté avec succès.");
                    EmailSender.sendEmail(demande.getEmail(), "Confirmation de candidature", "Votre demande de stage a été reçue avec succès.");
                })
                .addOnFailureListener(e -> showToast("Erreur lors de l'ajout du stage à Firestore: " + e.getMessage()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri selectedFileUri = data.getData();
                etCV.setText(selectedFileUri.toString());
            }
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
