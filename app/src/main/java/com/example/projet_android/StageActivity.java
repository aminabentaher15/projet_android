package com.example.projet_android;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.GridView;
import android.widget.ImageView;
import androidx.appcompat.widget.SearchView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class StageActivity extends AppCompatActivity {

    GridView gridview;
    CustomAdapter customAdapter;
    int images[] = {R.drawable.android, R.drawable.angular, R.drawable.powerbi, R.drawable.python, R.drawable.talend,R.drawable.devops,R.drawable.ia,R.drawable.spring,R.drawable.flutter,
            R.drawable.java,R.drawable.wordpress,R.drawable.android};
    String names[] = {"Android", "Angular", "Powerbi", "Python", "Talend","stage pfe DevOps","stage pfa IA","stage web","stage mobile","Gestion de stocks","Stage fullstack","developpement mobile"};
    String description[] = {"stage de 6 mois en développement d'applications mobiles Android contact:contact@companyA.com", "stage de 4 mois en développement web frontend en utilisant le framework JavaScript Angular", "stage de 4 mois en analyse de données contient une partie de dashbording avec powerbi", " stage de 6 mois en intelligence artificielle utlisant python comme langage de programmation", "concevoir, développer et exécuter des tâches d'intégration de donnée avec l'outils Talend","concevoir, développer et exécuter des tâches d'intégration de donnée avec l'outils Docker,K8S","concevoir, développer et exécuter des tâches d'intégration de donnée avec NLP","concevoir, développer et exécuter des tâches d'intégration de donnée avec Spring Boot et NodeJs",
            "stage pfe mobile de 6 mois en flutter","Participer à l'écriture de code HTML, CSS, JavaScript et éventuellement d'autres langages de programmation pour créer des pages web et des fonctionnalités interactives.","Wordpress, PHP, JAVASCRIPT, HTML, CSS,\n" +
            "Forte attention aux détails et capacité de travailler de manière autonome.\n" +
            "Curiosité technique","Stage 6mois en developpement mobile"};
    List<Stage> stageslist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage);
        gridview = findViewById(R.id.gridview);

        for (int i = 0; i < names.length; i++) {
            Stage stage = new Stage(names[i], description[i], images[i]);
            stageslist.add(stage);
        }
        customAdapter = new CustomAdapter(stageslist, this);
        gridview.setAdapter(customAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search_view);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                customAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_view) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class CustomAdapter extends BaseAdapter implements Filterable {
        private List<Stage> stageslist;
        private Context context;
        private List<Stage> stageslistfiltered;

        public CustomAdapter(List<Stage> stageslist, Context context) {
            this.stageslist = stageslist;
            this.context = context;
            this.stageslistfiltered = new ArrayList<>(stageslist);
        }

        @Override
        public int getCount() {
            return stageslistfiltered.size();
        }

        @Override
        public Object getItem(int position) {
            return stageslistfiltered.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(R.layout.row_items, parent, false);
            ImageView imageview = view.findViewById(R.id.imageview);
            TextView tvnames = view.findViewById(R.id.tvname);
            imageview.setImageResource(stageslistfiltered.get(position).getImage());
            tvnames.setText(stageslistfiltered.get(position).getName());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(StageActivity.this, ItemViewActivity.class);
                    intent.putExtra("stage", stageslistfiltered.get(position));
                    startActivity(intent);
                }
            });

            return view;
        }
        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint == null || constraint.length() == 0) {
                        filterResults.count = stageslist.size();
                        filterResults.values = stageslist;
                    } else {
                        String searchStr = constraint.toString().toLowerCase();
                        List<Stage> resultData = new ArrayList<>();
                        for (Stage stage : stageslist) {
                            if (stage.getName().toLowerCase().contains(searchStr)) {
                                resultData.add(stage);
                            }
                        }
                        filterResults.count = resultData.size();
                        filterResults.values = resultData;
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    stageslistfiltered = (List<Stage>) results.values;
                    notifyDataSetChanged();
                }
            };
            return filter;
        }
    }
}


