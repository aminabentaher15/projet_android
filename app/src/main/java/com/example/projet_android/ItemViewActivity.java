package com.example.projet_android;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemViewActivity extends AppCompatActivity {
    ImageView imageView;
    TextView textView , desc;
    Stage stage;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_view);
        imageView = findViewById(R.id.imageViewItem);
        textView = findViewById(R.id.tvNameItem);
        button = findViewById(R.id.button);
        desc =findViewById(R.id.tvdescription);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            stage = (Stage) extras.getSerializable("stage");

            if (stage != null) {
                imageView.setImageResource(stage.getImage());
                textView.setText(stage.getName());
                desc.setText(stage.getDescription());
            }
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the DemanderStageActivity
                Intent demandeStageIntent = new Intent(ItemViewActivity.this, DemanderStageActivity.class);
                startActivity(demandeStageIntent);
            }
        });
    }
}
