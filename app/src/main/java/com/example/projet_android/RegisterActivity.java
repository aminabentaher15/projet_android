package com.example.projet_android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText editTextName, editTextPassword, editTextPassword2;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        editTextName = findViewById(R.id.editTextName);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextPassword2 = findViewById(R.id.editTextPassword2);
        buttonLogin = findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Récupérer les valeurs saisies
                String email = editTextName.getText().toString();
                String password = editTextPassword.getText().toString();
                String confirmPassword = editTextPassword2.getText().toString();

                // Vérifier si les mots de passe correspondent
                if (!password.equals(confirmPassword)) {
                    showToast("Les mots de passe ne correspondent pas");
                    return;
                }

                // Créer le compte utilisateur avec Firebase Authentication
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Succès de la création du compte
                                    showToast("Compte créé avec succès");
                                    // Vous pouvez rediriger l'utilisateur vers une autre activité ici si nécessaire
                                } else {
                                    // Échec de la création du compte, afficher un message d'erreur
                                    showToast("Erreur lors de la création du compte : " + task.getException().getMessage());
                                }
                            }
                        });
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}