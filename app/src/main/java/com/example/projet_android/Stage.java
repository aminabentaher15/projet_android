package com.example.projet_android;

import java.io.Serializable;

public class Stage implements Serializable {
        public String name;
        public String description;
        public int image;

        public Stage() {
        }

    public Stage(String name, String description, int image) {
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
