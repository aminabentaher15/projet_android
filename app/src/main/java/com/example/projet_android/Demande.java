package com.example.projet_android;

public class Demande {
    private String nom;
    private String prenom;
    private String telephone;
    private String email;
    private String niveauEtude;
    private String specialite;
    private String cvPath;



    public Demande(String nom, String prenom, String telephone, String email, String niveauEtude, String specialite, String cvPath) {
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.email = email;
        this.niveauEtude = niveauEtude;
        this.specialite = specialite;
        this.cvPath = cvPath;
    }

    public Demande() {
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    public String getNiveauEtude() {
        return niveauEtude;
    }

    public String getSpecialite() {
        return specialite;
    }

    public String getCvPath() {
        return cvPath;
    }



    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNiveauEtude(String niveauEtude) {
        this.niveauEtude = niveauEtude;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public void setCvPath(String cvPath) {
        this.cvPath = cvPath;
    }


}
